# WIP: Auto-Uploader

Script to retrieve data written to google sheets thorugh the sheets API, and use ~~nightwatch~~ cypress to automate data transfer to resume upload website.
* nightwatch is having issues with retrieving elements from a poorly coded website that has no ID, typing etc

# TL;DR

1. Install Node and run `npm install`
2. Run `bash launch.sh`
3. Enter login details, API key/auth and spreadsheet ID
4. Enter cell range
5. Repeat

## Why?

The Mrs. does copywriting for work, and needs to upload copy into this website. It's not productive to write the copy directly into the website's tool, so she writes into word/sheets/docs and copy&paste after a days work.

Using this script I can retrieve copy written into sheets API, and upload to the website using cypress to automate the copy&paste process. 

# Auto-Uploader

Script to retrieve data written to google sheets thorugh the sheets API, and use cypress to automate data transfer to cat.bold.com

## Set up 

1. Download a zip file of the code from this page under the header
2. Open/unzip downloaded file and place in a known directory
3. Navigate to the directory in Finder
4. Open and Install 'node-v14.15.3.pkg'
5. While node is installing, open a terminal window at your folder location by navigating to the settings dropdown cog in Finder > New Terminal at Folder 

![alt text](img/terminal.png)

6. After node has installed, type 'node -v' into the terminal window you just opened. You should see the node version you just installed be returned in the console, similar to the below image (Note: If you literally type 'node -v' with the quotes you will get an error, when I use quotes here I mean to type what is inside the quotes, not the quoted string.) 

![alt text](img/node-v.png)

7. After this, type 'npm -v' in the terminal the same way you did before. You should see a version number returned in the console similar to above. We are just checking that node and npm installed correctly onto your machine, if you don't see either of these version numbers returned in your terminal console then something went wrong with the install and you should start again from Step 4
8. In the terminal, type 'npm install'. This will install some dependencies that are needed to run the script. You should see an output similar to below 

![alt text](img/npm.png)

9. That's it, done! You can leave the terminal window open to run the script in the next steps

### Before running the script

1. Ensure you have your Bold login details handy
2. Ensure your copy is written/pasted into Google Sheets and you know your spreadsheetID (can be found in the URL)
3. Ensure you have a copy of the API authorisation key 

#### To run the script

1. Ensure setup steps are complete and 'node -v' and 'npm -v' are correctly returned in the console before continuing
2. In the same terminal window you were using for the install, type 'bash launch.sh' 
3. On first setup, the script will need a couple of authentication details such as your login details for LiveCareer (user and pass) and the Google API authentication details (API and SpreadsheetID)
    - Google API will be provided by Verity over Slack. You can just Copy/Paste into the terminal
    - SpreadsheetID can be found in the URL of the spreadsheet shared to you between the d/ and the /edit. It's the weird looking encrypted string that looks similar to the API key
4. Cell range: Is referring to the range of cells with data in them that you would like to upload to Bold. The range is customizable, but the upload script is hard-coded to 12 Skills and 10 Experience; so you always need to select a range that reflects this. 
    - DO NOT select cells that have headings or formatting in them, because this will cause the data in these cells to be included in the script and uploaded to cat.bold.com
    - Also do not select only the cells with data in them. As we are uploading more skills than experience, we will actually need to select a cell range that includes some **blank** cells otherwise some of our skills will be cut off
    - Cell Range must be in the format of Range1:Range2 i.e. B2:C13

![alt text](img/range.png)

5. After the script has run, cypress will remain open so you can confirm the data and enter your Occupation/Job Title. After you have pressed Submit in cypress and you are happy with the results you can either:
    - &#8984 + W then &#8984 + Q to close the cypress window and quit cypress. We will relaunch cypress in the script and we need a fresh, clean copy of cypress so you always want to close and quit
    - In the terminal window, hold control and then press 'C'. This will "trap" the test and stop the code from running. This is exactly what we want since you have already submitted your titles and are done with that set
6. Repeat! You can repeat this as many times as possible. The script will save your authorisation and login details for the future, you just need to enter in a new cell range each time, or copy/paste into the same cell range and just repeat the test with the same values


