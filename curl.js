/*=========================================================================*/
 /* Main script to retrieve data from Google Sheets in a cell range and    */
 /* save to JSON file. Cypress can then read directly from the JSON data   */
 /*========================================================================*/

/**
 * require node files etc
 */
const fs = require('fs'); 

/**
 * require local files 
 */
const auth = require('./authData');
const {google} = require('googleapis');
const sheets = google.sheets({
  version: 'v4',
  auth: auth.api // provide auth from auth.json generated from launch.sh script
});

/**
 * main cURL request (skills & experience
 */
async function main () {
  const request = {
    // The ID of the spreadsheet to retrieve data from.
    spreadsheetId: auth.spreadsheet, // retrieve from auth.json

    // The A1 notation of the values to retrieve.
    range: process.argv[2],

    // How values should be represented in the output.
    // The default render option is ValueRenderOption.FORMATTED_VALUE.
    valueRenderOption: 'FORMATTED_VALUE', 

    // How dates, times, and durations should be represented in the output.
    // This is ignored if value_render_option is
    // FORMATTED_VALUE.
    // The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
    dateTimeRenderOption: 'FORMATTED_STRING', 
  };

  try {
    // await data
    let response = (await sheets.spreadsheets.values.get(request)).data;
    //write array to json
    fs.writeFile ("sheetsData.json", JSON.stringify(response), function(err) {
      if (err) throw err;
      console.log('2. Successfully retrieved data from Google Sheets API');
      return response; 
      });
  } catch (err) {
    console.error("ERROR! Could not retrieve data from Google Sheets API: ", err);
  }
}

/**
 * Run main function and WAIT for sheets API call
 */
main().then(() => {})
