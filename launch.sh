#!/bin/bash
#Echo some blurb
echo "/*=======================================================*/


This script will retrieve data from Google Sheets, and automate
the copy/paste process to cat.bold.com using browser automation."
echo #Blank echo for formatting
read -p "------------------------------------------------------ 
--------     Press any key to begin script     -------
------------------------------------------------------ " -n1 -s
echo #Blank echo to clear shell
#Set AUTH and Spreadsheet ID
echo
if [[ ! -f auth.json ]]; 
then read -p "------ Authorization does not exist ------
    Please enter Google Sheets API Key: " apiKey;
    read -p "    Please enter Spreadsheet ID: " spreadsheetId;
    echo "{\"api\":\"$apiKey\",\"spreadsheet\":\"$spreadsheetId\"}" > auth.json;
else echo "------ Authorization exists, reading from auth.json ------";
fi
echo #Blank echo to clear shell
#Set user & pass ENV VAR
if [[ ! -f cypress.env.json ]]; 
then read -p "------ Login credentials do not exist ------
    Please enter LiveCareer username " username;
    echo "     <<< Username is $username >>> ";
    read -p "    Please enter LiveCareer password " password;
    echo "     <<< Password is $password >>> ";
#Output to json
    echo "{\"user\":\"$username\",\"pass\":\"$password\"}" > cypress.env.json;
else echo "------ Login credentials exist, reading from cypress.env.json ------"
fi
#Set cell range
read -p "1. Please enter cell range: " range
echo "     <<< Cell range is $range >>> "
# Run curl script to retrieve API data
echo "2. Calling Google API"
node curl.js $range; # Change this to curl script
# Wait
sleep 2;
# Run cypress
echo "4. Launching cypress, !!! control + C to stop test at any time !!!";
sleep 4; #Wait to allow time to read shell
npm run cy:run -- --headed --no-exit --spec "cypress/integration/auto-upload/boldUpload.js";
echo "5. Success! Chrome and Cypress were exited";
exit 0
