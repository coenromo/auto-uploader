const username = Cypress.env('user');
const password = Cypress.env('pass');

describe ('Login Request', () =>{
    it('Visits site and perform AUTH', () => {

/**
 * read json file from curl.js request
 */

cy.request({
    method: 'POST',
    url: 'https://auth.livecareer.com/account/login?ReturnUrl=%2Foauth%2Fauthorize%3Fclient_id%3DCAT_PRD_W_COR%26response_type%3Dcode%26redirect_uri%3Dhttps%253A%252F%252Fcat.bold.com%252Fexternallogin%252Fauthorizationcodecallback%26scope%3Dread%26state%3D%252F%26ignore_grant%3Dtrue%26portal_cd%3DCAT', // baseUrl will be prepended to this url
    form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
    body: {
      username,
      password,
    },
  })
  cy.visit('cat.bold.com')
})
})

/**
 * Navigate to form 
 */
describe('Navigate to form', () => {
  it('Ensure form is in focus', () =>{
    //click add content button
    cy.get('button.btn.btn-primary').contains('Content')
    cy.contains('Add New Content').click() 

    //click maximize button
    cy.get('i.fas.fa-expand-alt.dark.m-0').click()

    //get Resume Wizard buttton
    cy.get('h1.card-title').contains('RWZ')
    cy.contains('RWZ').click()

    //click Write Content HEading - Write Content
    cy.get('h5.mb-0').contains('Write Content')
    cy.contains('Write Content').click()
  })
})

/**
 * Add data to form 
 */
describe('Fill out form', () => {
  it('Types into form SKILLS', () => {
    //try read file
    cy.readFile('sheetsData.json').then((data) => {
      //read data into array || skills now holds data from file in array
      var skills = data.values;
      /**
       * id 0
       */
      cy.get('div[id="0"]').eq(0).type(skills[0][0])
      /**
       * id 1
       */
      cy.get('div[id="1"]').eq(0).type(skills[1][0])
      /**
       * id 2
       */
      cy.get('div[id="2"]').eq(0).type(skills[2][0])
      /**
       * id 3
       */
      cy.get('div[id="3"]').eq(0).type(skills[3][0])
      /**
       * id 4
       */
      cy.get('div[id="4"]').eq(0).type(skills[4][0])
      /**
       * id 5
       */
      cy.get('div[id="5"]').eq(0).type(skills[5][0])
      /**
       * id 6
       */
      cy.get('div[id="6"]').eq(0).type(skills[6][0])
      /**
       * id 7
       */
      cy.get('div[id="7"]').eq(0).type(skills[7][0])
      /**
       * id 8
       */
      cy.get('div[id="8"]').eq(0).type(skills[8][0])
      /**
       * id 9
       */
      cy.get('div[id="9"]').eq(0).type(skills[9][0])
      /**
       * id 10
       */
      cy.get('div[id="10"]').eq(0).type(skills[10][0])
      /**
       * id 11
       */
      cy.get('div[id="11"]').eq(0).type(skills[11][0])
    })

  })
  it('Types into form EXPERIENCE', () => {
    //try read file
    cy.readFile('sheetsData.json').then((data) => {
      //read data into array || exp now holds data from file in array
      var experience = data.values;
      /**
       * id 0
       */
      cy.get('div[id="0"]').eq(1).type(experience[0][1])
      /**
       * id 1
       */
      cy.get('div[id="1"]').eq(1).type(experience[1][1])
      /**
       * id 2
       */
      cy.get('div[id="2"]').eq(1).type(experience[2][1])
      /**
       * id 3
       */
      cy.get('div[id="3"]').eq(1).type(experience[3][1])
      /**
       * id 4
       */
      cy.get('div[id="4"]').eq(1).type(experience[4][1])
      /**
       * id 5
       */
      cy.get('div[id="5"]').eq(1).type(experience[5][1])
      /**
       * id 6
       */
      cy.get('div[id="6"]').eq(1).type(experience[6][1])
      /**
       * id 7
       */
      cy.get('div[id="7"]').eq(1).type(experience[7][1])
      /**
       * id 8
       */
      cy.get('div[id="8"]').eq(1).type(experience[8][1])
      /**
       * id 9
       */
      cy.get('div[id="9"]').eq(1).type(experience[9][1])
    })
  })
})
