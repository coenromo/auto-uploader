/**
 * get chrome error on navigating to login due to load times and redirect
 * need to move this to before, then navigate to new url after cookies have been established
 */
describe('My First Test', () => {
  it('visits website', () => {
    cy.visit(process.env.URL)
    //cy.visit('URL')
    cy.get('input[type="email"]').type(process.env.EMAILBOLD) //
    cy.get('input[type="password"]').type(process.env.PASSWORDBOLD)
    cy.get('input[name="submit.Signin"]').click()
  })
})
