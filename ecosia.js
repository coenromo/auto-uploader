/**
 * require files etc
 */
const myModule = require('./curl.js');
const fs = require('fs');

// fs.readFile('inputTest.json', 'utf8' , (err, dataFile) => {
//   if (err) {
//     console.error(err)
//     return
//   }
//   console.log(dataFile, '     :dataFile RESULT');
//   //assign to global var
//   testVar = dataFile; 
// });

/**
 * read json file from curl.js request
 */
let rawdata = fs.readFileSync('inputTest.json');
let curlData = JSON.parse(rawdata);
console.log(curlData);

/**
 * start nightwatch tests
 */
describe('Ecosia.org Demo', function() {

    before(browser => browser.url(process.env.URL));
  
    test('Demo test BOLD', function (browser) {
      browser
        .waitForElementVisible('body')
        .assert.titleContains('Login')
        .waitForElementVisible('input[type=email]', 6000)
        .assert.visible('input[type=email]')
        .click('input[type=email]')
        .setValue('input[type=email]', process.env.EMAILBOLD)
        //.setValue('input[type=search]', curlData.values[0].toString())
        .assert.visible('input[type=password]')
        .setValue('input[type=password]', process.env.PASSWORDBOLD)
        //.assert.containsText('.mainline-results', 'Block Quotations')
        //.assert.containsText('input[type=email]', 'hello@shemediaco.com')
        .click('.btn-primary')
        .pause(5000)
        .assert.visible('.btn-primary')
    });

    test('Demo test AddNewContent', function (browser) {
      browser
        .assert.visible('.btn-primary')
        .useXpath().click("//*[contains(text(), 'Add New Content')]")
        .pause(3000)
        //.assert.visible('.fa-expand-alt')
        //.useXpath().click("//*[contains(text(), '.fa-expand-alt')]")
        //.useXpath().click("//*[contains(text(), 'Resume Builder')]") 
        
        .useXpath().click("//*[contains(text(), '2. ADD TAGS')]")

        //.assert.visible('input[id=headingTwo]')
        //.click('input[id=headingTwo]')
        //.assert.visible('input[id=jobTitleTypeAhead]')

        //.setValue('input[id=jobTitleTypeAhead]', curlData[10])
    });
  
    after(browser => browser.end());
  });
